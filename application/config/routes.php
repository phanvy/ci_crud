<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'homecontroller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['product'] = 'productcontroller/index';
$route['category'] = 'categorycontroller/index';
$route['category/create']['post'] = 'categorycontroller/create';
$route['category/edit/(:any)']['post'] = 'categorycontroller/update';
$route['category/delete/(:any)'] = 'categorycontroller/delete';