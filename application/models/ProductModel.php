<?php
/**
 * Created by PhpStorm.
 * User: vy
 * Date: 04/04/2019
 * Time: 10:02 SA
 */

class ProductModel extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }
    public function getAllProduct()
    {
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('products');
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }
}