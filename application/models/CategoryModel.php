<?php
/**
 * Created by PhpStorm.
 * User: vy
 * Date: 04/04/2019
 * Time: 10:05 SA
 */

class CategoryModel extends CI_Model
{
    public $name;
    public $imageCategory;
    public $description;
    public $is_deleted;
    public $create_at;
    public $update_at;

    public function __construct()
    {
        $this->load->database();
    }

    public function getAllCategories() {
      $this->db->where('is_deleted', 0)->order_by('id', 'desc');
      $query = $this->db->get('categories');
      if($query->num_rows() > 0){
          return $query->result();
      } else {
          return false;
      }
    }

    public function addNewCategory() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->name = $_POST['name'];
        if($_POST['image']){
//            Add code uploading image
            $config['upload_path']="assets/images/uploads/";
            $config['allowed_types']='gif|jpg|png';
            $this->load->library('upload',$config);

            $this->imageCategory = $_POST['image'];
        }else{
            $this->imageCategory = 'http://footballtvup.com/media/default-image.jpg';
        }
        $this->description = $_POST['description'];
        $this->is_deleted = 0;
        $this->create_at = date('Y-m-d H:i:s');

        $this->db->insert('categories', $this);
        echo 'Create Successful';
    }

    public function updateCategory() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->name = $_POST['name'];
        if($_POST['image']){
//            Add code uploading image
            $this->imageCategory = $_POST['image'];
        }else{
            $this->imageCategory = 'http://footballtvup.com/media/default-image.jpg';
        }
        $this->description = $_POST['description'];
        $this->is_deleted = 0;
        $this->updatedAt = date('Y-m-d H:i:s');
        $this->db->update('categories', $this, array('id'=>$_POST['id']));
    }

    public function deleteCategory() {
        $this->isDeleted = 1;
        //Add code to delete image in folder and update category's products.isDeleted = 1, categories.isDeleted = 1
    }
}