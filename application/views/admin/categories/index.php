<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Categories Management</h2>
            </div>
            <br>
            <div class="pull-right">
                <button class="btn btn-success" data-toggle="modal" data-target="#create_category">Create Category</button>
            </div>
        </div>
    </div>
    <br>
    <table class="table table-bordered" id="category_table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Image</th>
                <th>Description</th>
                <th>Create At</th>
                <th>Update At</th>
                <th width="200px">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php if($data) {
            foreach ($data as $item) { ?>
                <tr>
                    <td><?php echo $item->id; ?></td>
                    <td><?php echo $item->name; ?></td>
                    <td><img src="<?php echo $item->imageCategory ?>" class="img-thumbnail" alt="Category Image"></td>
                    <td><?php echo $item->description; ?></td>
                    <td><?php echo $item->create_at; ?></td>
                    <td><?php echo $item->update_at; ?></td>
                    <td style="text-align:center;">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#edit_category_<?php echo $item->id ?>">Edit</button>
                        <button type="button" class="btn btn-danger" value="<?php echo $item->id ?>">Delete</button>
                    </td>
                </tr>
                <!--Form edit category-->
                <form method="post" enctype="multipart/form-data" name="form_update">
                    <div class="modal fade" id="edit_category_<?php echo $item->id ?>" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Create new Category</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Category Name:</label>
                                        <div class="col-md-9">
                                            <input type="text" id="name_<?php echo $item->id ?>" class="form-control" value="<?php echo $item->name; ?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Category Image:</label>
                                        <div class="col-md-9">
                                            <input type="file" id="image_<?php echo $item->id ?>" required>
                                            <img src="<?php echo $item->imageCategory ?>" class="img-thumbnail" alt="Category Image">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Description:</label>
                                        <div class="col-md-9">
                                            <textarea id="description_<?php echo $item->id ?>" class="form-control" rows="3" value="<?php echo $item->description ?>" required><?php echo $item->description ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" id="btn_edit_<?php echo $item->id ?>" class="btn btn-primary submitData" value="<?php echo $item->id ?>">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            <?php }
        }?>
        </tbody>
    </table>
</div>
<!--Form create category-->
<form method="post" enctype="multipart/form-data" id="form_create">
    <div class="modal fade" id="create_category" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create new Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Category Name:</label>
                        <div class="col-md-9">
                            <input type="text" id="name" name="name" class="form-control" placeholder="Enter category name..." value="" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Category Image:</label>
                        <div class="col-md-9">
                            <input type="file" id="image" name="image" value="" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Description:</label>
                        <div class="col-md-9">
                            <textarea rows="3" id="description" name="description" class="form-control" placeholder="Select category's image..." value="" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" type="submit" id="btn_save" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        $('#category_table').DataTable({
            "pageLength" :5,
        });
    });
</script>
<!--Create category-->
<script type="text/javascript">
    $(document).on('click', '#btn_save', function(){
        var c_name = $('#name').val();
        var c_image = $('#image').val();
        var c_description = $('#description').val();
        if((c_name == "") || (c_image == "") || (c_description == "")){
            alert('Vui lòng nhập đầy đủ thông tin');
        } else {
            $.ajax({
                type: 'post',
                url: 'category/create',
                dataType: 'json',
                data: {
                    name: c_name,
                    image: c_image,
                    description: c_description,
                },
                success: function(data){
                    alert(data);
                    $("tbody").append(
                        "<tr>" +
                        "<td>"+"</td>" +
                        "<td>"+data.name+"</td>" +
                        "<td>"+data.image+"</td>" +
                        "<td>"+data.description+"</td>" +
                        "<td style="+"text-align:center;"+">"+
                        "<button type="+"button class=" + "btn btn-secondary" + "data-dismiss=" + "modal"
                        + ">" +Close+"</button>" +
                        "<button type=" + "button" +"type="+"submit id="+"btn_save class=" + "btn btn-primary>"+Save+"</button>"+
                        "</td>"+
                        "</tr>");
                    $('#create_category').modal('hide');
                }
            });
        }
    });
</script>

<!--Edit Category-->
<script type="text/javascript">
    $(document).on('click', '.submitData', function(){
        var cate_id = $(this).val();
        var cate_name = $('#name_' + cate_id).val();
        var cate_image = $('#image_' + cate_id).val();
        var cate_description = $('#description_' + cate_id).val();
        // $.ajaxSetup({
        //     headers:{
        //
        //     }
        // })
        $.ajax({
            type: 'post',
            url: 'category/edit/' + cate_id,
            dataType: 'json',
            data: {
                id: cate_id,
                name: cate_name,
                image: cate_image,
                description: cate_description,
            },
            success: function(data){
                alert(data);
                $("tbody").append(
                    "<tr>" +
                        "<td>"+data.id+"</td>" +
                        "<td>"+data.name+"</td>" +
                        "<td>"+data.image+"</td>" +
                        "<td>"+data.description+"</td>" +
                        "<td style="+"text-align:center;"+">"+
                            "<button type="+"button class=" + "btn btn-success" + "data-toggle=" + "modal data-target="
                            + "#edit_category_" +data.id+ ">" +Edit+"</button>" +
                            "<button type=" + "button class=" + "btn btn-danger value="+data.id+">"+Delete+"</button>"+
                        "</td>"+
                    "</tr>");
                $('#edit_category_'+data.id).modal('hide');
            }
        });
    });
</script>
<!--Validation-->
<script>
    $(document).ready(function() {
        $("#form_create").validate({
            rules: {
                name: "required",
                image: "required",
                description: {
                    required: true,
                    minlength: 6
                }
            },
            messages: {
                name: "Vui lòng nhập tên!",
                image: "Vui lòng chọn ảnh!",
                description: {
                    required: "Vui lòng nhập mô tả!",
                    minlength: "Mô tả quá ngắn!"
                }
            }
        });
    });
</script>

