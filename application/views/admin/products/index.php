<?php
/**
 * Created by PhpStorm.
 * User: vy
 * Date: 04/04/2019
 * Time: 10:10 SA
 */
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>CRUD Product</h2>
            </div>
            <br>
            <div class="pull-right">
                <a class="btn btn-success" id="btn_add">Add New Product</a>
            </div>
        </div>
    </div>
    <br>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>In_Stock</th>
            <th id="pro_img">Image</th>
            <th>Create_at</th>
            <th>Update_at</th>
            <th width="200px">Action</th>
        </tr>
        </thead>
        <tbody id="list_product">

        </tbody>
    </table>
</div>
<!-- Add new modal -->
<form role="form" id="addnewform">
    <div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Product Name*</label>
                        <div class="col-md-9">
                            <input type="text" name="txt_name" id="txt_name" class="form-control" placeholder="Product Name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Description*</label>
                        <div class="col-md-9">
                            <input type="text" name="txt_description" id="txt_description" class="form-control" placeholder="Description" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Price*</label>
                        <div class="col-md-9">
                            <input type="number" name="txt_price" id="txt_price" class="form-control" placeholder="Price" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Quantity*</label>
                        <div class="col-md-9">
                            <input type="number" name="txt_quantity" id="txt_quantity" class="form-control" placeholder="Quantity" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Product Image</label>
                        <div class="col-md-9">
                            <input type="file" name="txt_image" id="txt_image" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" type="submit" id="btn_save" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- End modal -->
<!-- Edit modal -->
<form>
    <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <!-- <label class="col-md-3 col-form-label">Product ID</label> -->
                        <div class="col-md-9">
                            <input type="hidden" name="product_id_edit" id="product_id_edit" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Product Name</label>
                        <div class="col-md-9">
                            <input type="text" name="product_name_edit" id="product_name_edit" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Description</label>
                        <div class="col-md-9">
                            <input type="text" name="product_des_edit" id="product_des_edit" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Price</label>
                        <div class="col-md-9">
                            <input type="number" name="product_pri_edit" id="product_pri_edit" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Quantity</label>
                        <div class="col-md-9">
                            <input type="number" name="product_quan_edit" id="product_quan_edit" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Product Image</label>
                        <div class="col-md-9">
                            <input type="file" name="txt_image" id="txt_image" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" type="submit" id="btn_update" class="btn btn-primary">Update</button>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- End modal -->
<!--Modal delete-->
<form>
    <div class="modal fade" id="modal_del" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <strong>Are you sure to delete this record?</strong>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="product_id_del" id="product_id_del" class="form-control" readonly>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="button" type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
                </div>
            </div>
        </div>
    </div>
</form>
<!--END MODAL DELETE-->
<script type="text/javascript">
    $(document).ready(function() {
            show_product();
            function show_product(){
                $.ajax({
                    type  : 'ajax',
                    url   : '<?php echo site_url('productcontroller/list_product')?>',
                    async : true,
                    dataType : 'json',
                    success : function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<tr>'+
                                '<td>'+data[i].id+'</td>'+
                                '<td>'+data[i].name+'</td>'+
                                '<td>'+data[i].description+'</td>'+
                                '<td>'+data[i].price+'</td>'+
                                '<td>'+data[i].in_stock+'</td>'+
                                '<td>'+data[i].image+'</td>'+
                                '<td>'+data[i].create_at+'</td>'+
                                '<td>'+data[i].update_at+'</td>'+
                                '<td style="text-align:right;">'+
                                '<a href="javascript:void(0)" class="btn btn-info item_edit" id="i_edit" data-id = "'+data[i].id+'" data-name = "'+data[i].name+'" data-description = "'+data[i].description+'" data-price = "'+data[i].price+'" data-quantity = "'+data[i].quantity+'" >Edit</a>'+' '+
                                '<a href="javascript:void(0);" class="btn btn-danger item_del" id="i_del" data-id = "'+data[i].id+'">Delete</a>'+
                                '</td>'
                            '</tr>';
                        }
                        $('#list_product').html(html);
                    }
                });
            }


            $('#btn_add').click(function()
            {
                $('#modal_add').modal('show');
            });
            $('#btn_save').click(function(){
                var pro_name = $('#txt_name').val();
                var pro_description = $('#txt_description').val();
                var pro_price = $('#txt_price').val();
                var pro_quantity = $('#txt_quantity').val();
                $.ajax({
                    type : "POST",
                    url  : "<?php echo site_url('product/addProduct/'); ?>",
                    dataType : "JSON",
                    data : {txt_name:pro_name , txt_description:pro_description,txt_price:pro_price,txt_quantity:pro_quantity},
                    success: function(data){
                        $('[name="txt_name"]').val("");
                        $('[name="txt_description"]').val("");
                        $('[name="txt_price"]').val("");
                        $('[name="txt_quantity"]').val("");
                        $('#modal_add').modal('hide');
                        show_product();
                    }
                });
                return false;
            });
            //get data for update record
            $('#list_product').on('click','.item_edit',function(){
                var product_id = $(this).data('id');
                var product_name = $(this).data('name');
                var product_desc = $(this).data('description');
                var product_price = $(this).data('price');
                var product_quan = $(this).data('quantity');
                console.log(product_id);

                $('#modal_edit').modal('show');

                $('[name="product_id_edit"]').val(product_id);
                $('[name="product_name_edit"]').val(product_name);
                $('[name="product_des_edit"]').val(product_desc);
                $('[name="product_pri_edit"]').val(product_price);
                $('[name="product_quan_edit"]').val(product_quan);
            });
            $('#btn_update').on('click',function(){
                var product_id = $('#product_id_edit').val();
                var pro_name = $('#product_name_edit').val();
                var pro_description = $('#product_des_edit').val();
                var pro_price = $('#product_pri_edit').val();
                var pro_quantity = $('#product_quan_edit').val();
                $.ajax({
                    type : "POST",
                    url  : "<?php echo site_url('product/update')?>",
                    dataType : "JSON",
                    data : {
                        product_id:product_id ,
                        pro_name:pro_name,
                        pro_description:pro_description,
                        pro_price:pro_price,
                        pro_quantity:pro_quantity
                    },
                    success: function(data)
                    {
                        $('[name="product_name_edit"]').val("");
                        $('[name="product_des_edit"]').val("");
                        $('[name="product_pri_edit"]').val("");
                        $('[name="product_quan_edit"]').val("");
                        $('#modal_edit').modal('hide');
                        show_product();
                    }
                });
                return false;
            });
            $('#list_product').on('click','.item_del',function(){
                var product_id = $(this).data('id');
                $('#modal_del').modal('show');
                $('[name="product_id_del"]').val(product_id);
                console.log(product_id);
            });
            $('#btn_delete').on('click',function(){
                    var product_id = $('#product_id_del').val();
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo site_url('product/delete')?>",
                        dataType : "JSON",
                        data : {
                            product_id:product_id,
                        },
                        success: function(data)
                        {
                            $('[name="product_id_del"]').val("");
                            $('#modal_del').modal('hide');
                            show_product();
                        }
                    });
                    return false;
                }
            );
        }
    )
</script>
