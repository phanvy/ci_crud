<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: khoa.leanh
 * Date: 4/5/2019
 * Time: 11:11 AM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>CodeInigter - CRUD</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.validate.min.js') ?>"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
</head>
<body>
    <div class="header_meta fixed-top ">
        <nav class="navbar navbar-expand-sm ">
            <button class="navbar-toggler btn btn-outline-dark" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <a class="navbar-brand text" href="#">
                    Logo here
                </a>
                <ul class="navbar-nav ">
                    <li class="nav-item">
                        <a class="nav-link text" href="<?php echo base_url('/') ?> ">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text" href="<?php echo base_url('product') ?>">Products</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text" href="<?php echo base_url('category') ?> ">Category</a>
                    </li>
                </ul>
            </div>
        </nav>
        <br>
    </div>
    <?php $this->load->view($subview); ?>
</body>
</html>
