<?php
/**
 * Created by PhpStorm.
 * User: vy
 * Date: 04/04/2019
 * Time: 9:59 SA
 */

class HomeController extends CI_Controller
{
    public function __construct() {
        parent::__construct();
    }
    public function index()
    {
        $this->load->view('layouts/header');
        $this->load->view('home/index');
        $this->load->view('layouts/footer');
    }
}