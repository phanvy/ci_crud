<?php
/**
 * Created by PhpStorm.
 * User: vy
 * Date: 04/04/2019
 * Time: 9:59 SA
 */

class ProductController extends CI_Controller
{
    public function __construct() {
        //load database in autoload libraries
        parent::__construct();
        $this->load->model('ProductModel','pro');
    }
    public function index()
    {
        $data['subview'] = 'admin/products/index';
        $this->load->view('admin/admin', $data);
    }
    //Get all product in db
    public function list_product()
    {
        $data=$this->pro->getAllProduct();
        echo json_encode($data);
    }
}