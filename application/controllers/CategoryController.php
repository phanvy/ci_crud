<?php
/**
 * Created by PhpStorm.
 * User: vy
 * Date: 04/04/2019
 * Time: 10:00 SA
 */

class CategoryController extends CI_Controller
{
    public function __construct() {
        //load database in autoload libraries
        parent::__construct();
        $this->load->model('CategoryModel','cat');
    }
    public function index()
    {
        $data['data'] = $this->cat->getAllCategories();
        $data['subview'] = 'admin/categories/index';
        $this->load->view('admin/admin', $data);

    }

    public function create(){
        $this->cat->addNewCategory();
    }

    public function update() {
        $this->cat->updateCategory();
    }

    public function delete() {
        $this->cat->deleteCategory();
    }
}